const LINKS = [
	{
		title: 'Home',
		scrollTo: 'home',
		visible: true
	},
	{
		title: 'Our work',
		scrollTo: 'projects',
		visible: true
	},
	{
		title: 'We work with',
		scrollTo: 'reports',
		visible: true
	},
	{
		title: 'Contact',
		scrollTo: 'contact',
		visible: true
	}
]

export default LINKS;
